//var electron = require("electron");
//var app = electron.app;  // Module to control application life.
//var BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.

var fs = require("fs");
var http = require("http");
var Stream = require('stream').Transform;   
var imagesInBase64 = [];

function readFiles(filename) {
    var file = fs.readFileSync(filename, 'utf8', function(err, data) {
        if (err) {
            throw err;
        }
        //if(data) {
            //\<\img\s+?src=\"(.+)?\"\s+\s*
            //callback(data);
        //}
    });
    return file;
}

function writeFiles (content) {
    console.log(content);
    fs.writeFile('home.html', content, 'utf8',  function (err) {
        if (err) return console.log(err);
    });
}

function sliceDOM(tag, data, concat) {
    var tag = 'head'
    var from = data.indexOf('>', data.search('<'+tag+'.*>'))+1;
    var strBody = data.substr(0, from);
    strBody += concat;
    var second = data.substr(from, data.length);
    strBody += second;
    return strBody;
}

function convertToBase64(file, callback) {

    console.log("existsSync", fs.existsSync(file));

    if(fs.existsSync(file)) {
        var fileData = fs.readFileSync(file);
        var imgInBase64 = new Buffer(fileData, 'binary').toString('base64');
        return imgInBase64;
    }

    console.log("NAO SYNC");

    http.get(file, function(response) {
        if (response.statusCode === 200) {
            var data = new Stream();                                                    
            response.on('data', function(chunk) {                                       
                data.push(chunk);                                                         
            });                                                                         

            response.on('end', function() {                                             
                var imgInBase64 = new Buffer(data.read(), 'binary').toString('base64');
                callback(imgInBase64);
                return;
            });                        
        } else {
            console.log("Erro ao carregar...." + response.statusCode);
        }

    });

    return;
}

function getImages(url, outPutName) {
    var http = require('http');
    var imagedata = [];
    http.get(url, function(res){
        res.setEncoding('binary');
        res.on('data', function(chunk){
            imagedata.push(chunk);
        })
        res.on('end', function(){
            fs.writeFile(outPutName, imagedata, 'binary', function(err){
                if (err) throw err
                console.log('File saved.');
            });
        });
    });
}

function downloadImages () {
    var http = require('http'),                                                
    url = 'http://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97350&w=350&h=350&fm=png';                    
    Stream = require('stream').Transform,                                  
    fs = require('fs');                                                    

    http.request(url, function(response) {                                        
        var data = new Stream();                                                    
        response.on('data', function(chunk) {                                       
            data.push(chunk);                                                         
        });                                                                         

        response.on('end', function() {                                             
            fs.writeFileSync('teste.png', data.read());                               
        });                                                                         
    }).end();
}

function getFiles(file) {
    http.get(file, function(res) {
        console.log("Got response: " + res.statusCode);
    }).on('error', function(e) {
        console.log("Got error: " + e.message);
    });
}

function parseHTML(html, callback) { 

    //var bg = /([\s\S]*?)
    
    //(url\(([^)]+)\))
    
    //(?!\s*[;,]?\s*\/\*\s*base64:skip\s*\*\/)
    //|

    //([\s\S]+)/img;
    
 //   for (var key in bg) {
        //console.log(readFiles(bg[3]));
        
    //}

    //var match = html.exec(/(url\(([a-z0-9]*\.png)?\))/img);

    var myRe = /([\s\S]*?)(url\(([^)]+)\))(?!\s*[;,]?\s*\/\*\s*base64:skip\s*\*\/)|([\s\S]+)/gmi;
    //var str = 'This is a hello world hello aqui hello !';
    var myArray;
    while ((myArray = myRe.exec(html)) !== null) {
      var msg = 'Found ' + myArray[3] +' ';

      var file = convertToBase64(myArray[3]);


      html = html.replace(myArray[3], 'data:image/png;base64,'+file);
      msg += 'Next match starts at ' + myRe.lastIndex;
      console.log(msg);
    }


    callback(html);
    return;



//    for (var i = 0; i < match.length; i++) {
        
  //      console.log("AQUI", match[1]);
        //match[i] = match[i].replace('url(','');
        //match[i] = match[i].replace(')','');
        //var base64 = convertToBase64(match[i], function (data){});
        //match[i] = base64;
        
    //};

    var cheerio = require('cheerio'),
    $ = cheerio.load(html);

    //console.log($);
    
    getImagesInBase64($, function(imagesInBase64) {
        $('img').each(function(i, elem) {
            console.log("IMAGESINBASE64", imagesInBase64.length, i);
            $(this).attr('src', 'data:image/png;base64,'+imagesInBase64[i]);
        });
        var path = $('script').attr('src');
        $('script').attr('src', null);
        $('script').text(readFiles(path).replace(/\s/g, ''));
        callback($html());
    });
}

function getImagesInBase64($, callback) {
    var total = $('img').length;
    var count = 0;
    $('img').each(function(i, elem) {
        var imgPath = $(this).attr('src');
        console.log(imgPath);
        convertToBase64(imgPath, function (data){  
            imagesInBase64.push(data.toString());
            count++;
            if (count === total) {
                callback(imagesInBase64);
            }
        });

    });
}

var fileContent = readFiles('index.html');
//var bg_url = /([\s\S]*?)(url\(([^)]+)\))(?!\s*[;,]?\s*\/\*\s*base64:skip\s*\*\/)|([\s\S]+)/img.exec(fileContent);
  //  console.log(bg_url);

/*console.log(typeof fileContent, fileContent);
var match = fileContent.match(/url\(([a-z0-9]*\.png)?\)/img);
console.log(match);*/

parseHTML(fileContent, function(html) {
    writeFiles(html);
});

//getImages('http://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97350&w=350&h=350&fm=png', 'logotipo');